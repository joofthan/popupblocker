class Robot{
    constructor(path, offsetX, offsetY){
        this.path = path;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }
    async click(x,y){
        const { exec } = require('child_process');
        exec("java -jar "+this.path+" "+(x+this.offsetX) + " " + (y+this.offsetY), (err, stdout, stderr) => {
        if (err) {
            throw new Error("You have to build maven project \"mouse\" first and then copy mouse.jar to folder: \""+this.path+"\" . "+"Could not execute mouse.jar with: "+x + " " + y+".");
        }else{
            //throw new Error("Executed");
        }

        // the *entire* stdout and stderr (buffered)
        //console.log(`stdout: ${stdout}`);
        //console.log(`stderr: ${stderr}`);
        });
    }

}

module.exports = Robot;