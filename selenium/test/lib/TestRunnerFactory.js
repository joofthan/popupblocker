const webdriver = require('selenium-webdriver');
const Addon = require("./Addon");
const { until } = require('selenium-webdriver');
const { By, Builder } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const chrome = require('selenium-webdriver/chrome');
const fs = require('fs');

const assert = require('assert');
const path = require('path');
const Page = require("./Page");
const Robot = require('./Robot');
const TestRunner = require('./TestRunner');

class TestRunnerFactory{
    static async create(browserName, debug = false){
        //let driver = await ;
        let driver;
        let config;
        if(browserName ==="firefox"){
            driver = await TestRunnerFactory.createFirefox('../../bin/popupblocker.xpi');
            config = Addon.firefoxElementConfig;
        }else if(browserName == "chrome"){
            driver = await TestRunnerFactory.createChrome('todo');
            config = Addon.chromeElementConfig;
        }else{
            throw new Error("Browser \""+browserName+"\" not supported");
        }

        let robot = new Robot(path.resolve(__dirname, "../../bin/mouse.jar"), TestRunner.offsetX, TestRunner.offsetY);
        let addon = await Addon.create(driver, robot, config);
        let runner = new TestRunner(driver, robot, addon, debug);
        await runner.initialize('file:///'+path.resolve(__dirname,'../../index.html'));
        return runner;
    }

    static async createFirefox(addonPath) {
        var service = new firefox.ServiceBuilder(path.resolve(__dirname, '../../bin/geckodriver.exe'));
        let driver = new webdriver.Builder().forBrowser('firefox').setFirefoxService(service).build();
        const firefoxExt = path.resolve(__dirname, addonPath);
        await new firefox.Driver(driver.getSession(), driver.getExecutor()).installAddon(firefoxExt, true);
        return driver;
    }
    static async createChrome(addonPath) {
        var service = new chrome.ServiceBuilder(path.resolve(__dirname, '../../bin/chromedriver.exe'));
        const chromeExt = path.resolve(__dirname, '../../bin/target.crx');
        let options = new chrome.Options().addExtensions(fs.readFileSync(chromeExt, { encoding: 'base64' })).addArguments("--disable-crash-reporter","--disable-in-process-stack-traces","--disable-logging","--disable-dev-shm-usage","--log-level=3","--output=/dev/null");
        let driver = new webdriver.Builder().forBrowser('chrome').setChromeService(service).setChromeOptions(options).build();
        return driver;
    }

}

module.exports = TestRunnerFactory;