# Popupblocker
## Build
### Required Software
- Linux
- NPM

### Build Firefox
```
npm run build
```
### Build Chrome
```
npm run build-chrome
```
## Test
```
npm run test
```

## Selenium Test (deprecated)
###  Required Software
- Windows
- NPM
- Maven
- Firefox
- Selenium

### Run Selenium Tests
```
cd selenium
npm run firefox
```