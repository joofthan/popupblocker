class Main {
    run(plattform:Plattform) {
        plattform.setDevMode(false);


        var tabHandler = plattform.getTabHandler();

        var mybrowser = plattform.getBrowserName();
        var browser = plattform.getBrowser();
        var newtabstring = plattform.getNewTabString();

        log("Browser: " + mybrowser);
        if(mybrowser === "chrome"){
            browser.contentSettings.popups.set({"primaryPattern":"<all_urls>", "setting":"allow"});
        }

        const WINDOW_BLOCK_DELAY = 10000;
        const TAB_BLOCK_DELAY = 4000;
        const TAB_BLOCK_DELAY_ONLY_ONE_TAB = 2000;

//notifydelays
        const NOTIFY_DELAY = 30000;

        var currenttabblocksetting;
        var currentblockwindows;
        let state = new State();

//Timeout and Interval
        let mytimer;
        let countIntervall;
        let enabletimer;


//storage

        let settings = new Settings();
        let listmanager = new ListManager(plattform.logger);


//testumgebung
        function openBlackwhitelist() {
            var createData = {
                url: "resources/blackwhitelist.html"
            };
            var creating = browser.tabs.create(createData);
        }


//----------------------get current url----------------------------------------------

//------------------window changed
        browser.windows.onFocusChanged.addListener((windowId) => {
            //log("Newly focused window: " + windowId);
            tabHandler.getActiveWindow(logTabs);
        });

        function logTabs(tabs) {
            for (let tab of tabs) {
                saveActiveTabInfo(tab);
            }
        }

//------------------tab changed
        function handleActivated(activeInfo) {
            if (mybrowser === "chrome") {
                browser.tabs.get(activeInfo.tabId, saveActiveTabInfo);
            } else {
                var gettingInfo = browser.tabs.get(activeInfo.tabId);
                gettingInfo.then(saveActiveTabInfo, onError);
            }

        }

        browser.tabs.onActivated.addListener(handleActivated);

        function isInTargetAllowlist(tabInfo) {
            return listmanager.inList("advancedwhitelist", tabInfo.url);
        }

//------------------url changed
        function handleUpdated(changedTabId, changeInfo, tabInfo) {
            if (changedTabId === state.myActiveTab) {
                saveActiveTabInfo(tabInfo);
            }

            if (state.isWatching && changedTabId === state.myWatchTab) {//wait for tab url than run main
                if (tabInfo.url !== newtabstring) {
                    log(tabInfo.url);
                    state.isWatching = false;
                    log("stop watching: tabid=" + tabInfo.id);
                    if (isInTargetAllowlist(tabInfo)) {
                        allowWindow(tabInfo.windowId);
                    } else {
                        handleNewTabOrWindow(tabInfo);
                    }

                }
            }
        }

        browser.tabs.onUpdated.addListener(handleUpdated);


//----------save collected info----------------
        function saveActiveTabInfo(tabInfo) {
            log("tabActive: tabid=" + tabInfo.id);
            if (tabInfo.url !== newtabstring) {
                state.myActiveTab = tabInfo.id;
                state.myCurrentUrl = tabInfo.url;
            }

            //log(myActiveTab);
            //log(myCurrentUrl);
        }

//----------------------get current url end----------------------------------------------


        browser.tabs.onCreated.addListener((tab) => {

            //log(currenttabblocksetting);
            //log(myCurrentUrl);

            if (isEnabled()) {


                var d = new Date();
                state.lastUrlChange = d.getTime();
                if (listmanager.inList("blacklist", state.myCurrentUrl)) {
                    currenttabblocksetting = settings.tabblocksettingBlacklist;
                    currentblockwindows = settings.blockwindowsBlacklist;
                } else if (listmanager.inList("whitelist", state.myCurrentUrl)) {
                    currenttabblocksetting = settings.tabblocksettingWhitelist;
                    currentblockwindows = settings.blockwindowsWhitelist;
                } else {
                    currenttabblocksetting = settings.tabblocksetting;
                    currentblockwindows = settings.blockwindows;
                }


                if (settings.waitforurl && !state.isWatching) {
                    state.isWatching = true;
                    log("start watching: tabid=" + tab.id);
                    state.myWatchTab = tab.id;
                } else {
                    handleNewTabOrWindow(tab);
                }

            } else {
                allowWindow(tab.windowId);
            }

        });


        function blockTabBecauseOfRecentlyOpened(tab) {
            log("blockTabBecauseOfRecentlyOpened: " + tab.id);
            showNotify("tab");
            closeRecentTabs();
            blockTab(tab.id);
        }

        function markTabAsRecentlyOpened(tab) {
            var d = new Date();
            state.noNewTabTill = d.getTime() + TAB_BLOCK_DELAY_ONLY_ONE_TAB;
            state.recentOpenedTabs.push(tab.id);
            setTimeout(function () {
                state.recentOpenedTabs.pop();
            }, TAB_BLOCK_DELAY_ONLY_ONE_TAB + 1000);
        }

        function handleNewTabOrWindow(tab) {
            log("handleNewTabOrWindow: tabid=" + tab.id);
            log("Tab url: "+tab.url)
            if (isTabInAllowedWindow(tab)) {
                if (tab.url === newtabstring || settings.waitforurl) {
                    handleNewTab(tab);
                } else {
                    log("UserOpenedTab");
                }
            } else {
                handleNewWindow(tab);
            }
        }

        function handleNewTab(tab) {
            log("handleNewTab: tabid=" + tab.id);
            let d = new Date();
            log("currenttabblocksetting: " + currenttabblocksetting);
            if (currenttabblocksetting === "allow") {
            } else if (currenttabblocksetting === "block") {
                showNotify("tab");
                blockTab(tab.id);
            } else if (currenttabblocksetting === "smart") {
                if (state.noNewTabTill > d.getTime()) {
                    blockTabBecauseOfRecentlyOpened(tab);
                } else {//no window or tab blocked in last "windowBlockDelay" miliseconds
                    markTabAsRecentlyOpened(tab);
                }
            } else if (currenttabblocksetting === "smartlight") {
                if (state.noNewTabTill > d.getTime() && state.lastBlockedWindow + WINDOW_BLOCK_DELAY > d.getTime()) {//in smartlight no tab blocking on util tab(only on util window)
                    blockTabBecauseOfRecentlyOpened(tab);
                } else {//no window blocked in last "windowBlockDelay" miliseconds
                    markTabAsRecentlyOpened(tab);
                }
            }
        }

        async function handleNewWindow(tab) {
            log("handleNewWindow: tabid=" + tab.id);
            if (currentblockwindows) {
                let windowCount = await tabHandler.getWindowCount();
                log(windowCount);
                if(windowCount <= 1){
                    allowWindow(tab.windowId);
                    return;
                }
                showNotify("window");
                closeRecentTabs();//this also closes tabs of until windows in smartlight mode
                blockTab(tab.id);
                let d = new Date();
                state.noNewTabTill = d.getTime() + TAB_BLOCK_DELAY;
                state.lastBlockedWindow = d.getTime();
            } else {
                allowWindow(tab.windowId);
            }
        }


        function closeRecentTabs() {
            log("closeRecentTabs");
            var d = new Date();
            if (state.noNewTabTill > d.getTime()) {
                for (var i = 0; i < state.recentOpenedTabs.length; i++) {
                    blockTab(state.recentOpenedTabs[i]);
                }
            }
        }

        function blockTab(tabId) {
            log("blockTab: close: tabid=" + tabId);
            browser.tabs.remove(tabId);

            clearTimeout(mytimer);
            mytimer = setTimeout(function () {
                hideNotify();
            }, NOTIFY_DELAY);
        }

        function handleDetached(tabId, detachInfo) {
            if (mybrowser === "chrome") {
                browser.tabs.get(tabId, onGot);
            } else {
                var gettingInfo = browser.tabs.get(tabId);
                gettingInfo.then(onGot, onError);
            }
        }

        browser.tabs.onDetached.addListener(handleDetached);

        function onGot(tabInfo) {
            allowWindow(tabInfo.windowId)
        }

        function onError(error) {
            throw new Error("Please use TabHandler.xyz()" + error);
        }


        function isEnabled() {
            return settings.enabled;
        }

        function enable(bool) {
            settings.enabled = bool;
            if (settings.enabled) {
                clearInterval(countIntervall);
                browser.browserAction.setBadgeText({text: ""});
                setIcon("default");
                browser.browserAction.setPopup({
                    popup: "resources/popups/setting.html"
                });
            } else {
                clearTimeout(mytimer);
                //setAllowNext(false);
                browser.browserAction.setBadgeText({text: ""});
                setIcon("disabled");
                browser.browserAction.setPopup({
                    popup: "resources/popups/disabled.html"
                });
            }
        }

        function disableTemp(sec) {
            state.countSec = sec;
            clearTimeout(mytimer);
            enable(false);
            countIntervall = setInterval(function () {
                countDown()
            }, 1000);
            countDown();
        }

        function countDown() {
            if (state.countSec <= 0) {
                enable(true);
            } else {
                browser.browserAction.setBadgeText({text: "" + state.countSec});
                state.countSec--;
            }
        }

        /*
        function NextIsAllowed(){
            var t = isNextAllowed;
            isNextAllowed = false;
            return t;
        }
        */

        /*
        function setAllowNext(allowbool){
            clearTimeout(mytimer);
            isNextAllowed = allowbool;
            browser.browserAction.setBadgeText({text: ""});
            if(allowbool){
                setIcon("green");
                browser.browserAction.setPopup({
                    popup: "resources/popups/description.html"
                });
            }
            else {
                setIcon("default");
                browser.browserAction.setPopup({
                    popup: "resources/popups/setting.html"
                });
            }
        }
        */

        function allowWindow(windowId) {
            state.allowedWindows[windowId] = "allowed";
        }

        function isTabInAllowedWindow(tab) {
            if (state.allowedWindows[tab.windowId] === "allowed") {
                log("isTabInAllowedWindow: true");
                return true;
            } else {
                log("isTabInAllowedWindow: false");
                return false;
            }
        }

        function showNotify(typestring) {
            if (typestring === "tab") {
                browser.browserAction.setPopup({
                    popup: "resources/popups/blockedtab.html"
                });
            } else {
                browser.browserAction.setPopup({
                    popup: "resources/popups/blocked.html"
                });
            }
            setIcon(typestring);
        }

        function hideNotify() {
            //browser.browserAction.setBadgeText({text: ""});
            setIcon("default");
            browser.browserAction.setPopup({
                popup: "resources/popups/setting.html"
            });
        }

        function setIcon(iconstring) {

            switch (iconstring) {
                case "default":
                    browser.browserAction.setIcon({
                        path: {
                            38: "resources/icons/icon-38.png",//firefox chrome
                            40: "resources/icons/icon-40.png",//edge
                            96: "resources/icons/icon-96.png" //firefox chrome
                        }
                    });
                    break;
                case "disabled":
                    browser.browserAction.setIcon({
                        path: {
                            38: "resources/icons/icon-38-d.png",//firefox chrome
                            40: "resources/icons/icon-40-d.png",//edge
                            96: "resources/icons/icon-96-d.png" //firefox chrome
                        }
                    });
                    break;
                case "window":
                    browser.browserAction.setIcon({
                        path: {
                            38: "resources/icons/icon-38-a.png",//firefox chrome
                            40: "resources/icons/icon-40-a.png",//edge
                            96: "resources/icons/icon-96-a.png" //firefox chrome
                        }
                    });
                    break;
                case "tab":
                    browser.browserAction.setIcon({
                        path: {                                //warnung icon nur 38
                            38: "resources/icons/icon-38-t.png",//firefox chrome
                            40: "resources/icons/icon-40-a.png",//edge
                            96: "resources/icons/icon-96-a.png" //firefox chrome
                        }
                    });
                    break;
            }

        }

        function onGotAddBlacklist(tabs) {
            for (let tab of tabs) {
                let turl = listmanager.getClearedUrl(tab.url);
                let ttitle = "";
                let tmessage = "";
                if (listmanager.inList("blacklist", turl)) {
                    ttitle = "Already in Blocklist!";
                    tmessage = '"' + turl + '" already exists in  Blocklist.';
                } else if (listmanager.inList("whitelist", turl)) {
                    ttitle = "Already in Allowlist!";
                    tmessage = '"' + turl + '" already exists in Allowlist.';
                }
                if (tmessage !== "") {
                    browser.notifications.create("isinlist", {
                        "type": "basic",
                        "iconUrl": browser.extension.getURL("resources/icons/icon-96-a.png"),//TODO: extension.getURL is deprecated
                        "title": ttitle,
                        "message": tmessage
                    });
                    return false;
                }
                //wenn noch nicht vorhanden
                listmanager.addUrl("blacklist", tab.url);
                let temlist = listmanager.getList("blacklist");

                browser.storage.sync.set({
                    blacklist: temlist
                });
                browser.notifications.create("isinlist", {
                    "type": "basic",
                    "iconUrl": browser.extension.getURL("resources/icons/icon-96.png"),
                    "title": "Added to Blocklist",
                    "message": '"' + turl + '"was successfuly added to Blocklist.'
                });
                currenttabblocksetting = "block";
                currentblockwindows = true;
                //clearTimeout(mylisttimer);
                //mylisttimer = setTimeout(function(){ currenttabblocksetting = tabblocksetting; currentblockwindows = blockwindows;}, listblockdelay);

            }
        }

        function onGotAddWhitelist(tabs) {

            for (let tab of tabs) {
                let turl = listmanager.getClearedUrl(tab.url);
                let ttitle = "";
                let tmessage = "";
                if (listmanager.inList("blacklist", turl)) {
                    ttitle = "Already in Blocklist!";
                    tmessage = '"' + turl + '" already exists in  Blocklist.';
                } else if (listmanager.inList("whitelist", turl)) {
                    ttitle = "Already in Allowlist!";
                    tmessage = '"' + turl + '" already exists in  Allowlist.';
                }
                if (tmessage !== "") {
                    browser.notifications.create("isinlist", {
                        "type": "basic",
                        "iconUrl": browser.extension.getURL("resources/icons/icon-96-a.png"),
                        "title": ttitle,
                        "message": tmessage
                    });
                    return false;
                }
                //wenn noch nicht vorhanden
                listmanager.addUrl("whitelist", tab.url);
                let temlist = listmanager.getList("whitelist");

                browser.storage.sync.set({
                    whitelist: temlist
                });

                browser.notifications.create("isinlist", {
                    "type": "basic",
                    "iconUrl": browser.extension.getURL("resources/icons/icon-96.png"),
                    "title": "Added to Allowlist",
                    "message": '"' + turl + '"was successfuly added to Allowlist.'
                });
                currenttabblocksetting = "allow";
                currentblockwindows = false;
                //clearTimeout(mylisttimer);
                //mylisttimer = setTimeout(function(){ currenttabblocksetting = tabblocksetting; currentblockwindows = blockwindows;}, listblockdelay);

            }
        }

        function handleAddonButtonClicks(changes) {
            if (changes["addonfunction"] !== undefined) {

                if (changes["addonfunction"].newValue === "addBlacklist") {
                    tabHandler.getActiveTab(onGotAddBlacklist);
                } else if (changes["addonfunction"].newValue === "addWhitelist") {
                    tabHandler.getActiveTab(onGotAddWhitelist);
                } else if (changes["addonfunction"].newValue === "allowNext" || changes["addonfunction"].newValue === "allowNextinSetting") {
                    disableTemp(20);
                } else if (changes["addonfunction"].newValue === "moresetting") {
                    //openBlackwhitelist();
                    disableTemp(1);
                    browser.runtime.openOptionsPage();
                } else if (changes["addonfunction"].newValue === "cancel") {
                    //setAllowNext(false);
                } else if (changes["addonfunction"].newValue === "enable") {
                    enable(true);
                } else if (changes["addonfunction"].newValue === "disable") {
                    clearTimeout(enabletimer);
                    enable(false);
                } else if (changes["addonfunction"].newValue === "feedback") {
                    disableTemp(1);
                    var createData = {
                        url: "http://www.popupblocker.joofthan.com/feedback/"
                    };
                    var creating = browser.tabs.create(createData);
                }

               if(mybrowser === "chrome"){
                if(changes["addonfunction"].newValue !== null){
                    browser.storage.local.set({
                        addonfunction:  null
                    });
                }
               }
            }
        }

        function StorageChange(changes, area) {
            if (changes["tabblocksetting"] !== undefined) {
                settings.tabblocksetting = changes["tabblocksetting"].newValue;
                currenttabblocksetting = settings.tabblocksetting;
            }
            handleAddonButtonClicks(changes);

            if (changes["blockwindows"] !== undefined) {
                settings.blockwindows = changes["blockwindows"].newValue;
            }
            listmanager.loadLists(changes);
            settings.loadSyncedListSettings(changes);

        }

        function onSyncedStorageRecieved(item) {//init3
            settings.loadSyncedSettings(item);
            listmanager.loadSyncedSettings(item);
            currentblockwindows = settings.blockwindows;
            browser.storage.onChanged.addListener(StorageChange);
        }


        function onLocalStorageReceived(item) {//init2
            settings.loadLocalSettings(item);
            currenttabblocksetting = settings.tabblocksetting;

            if (item["addonstatus"] === "disabled") enable(false);
            else enabletimer = setTimeout(function () {
                enable(true);
            }, 10000);

            //start init3
            if (mybrowser === "chrome") {
                browser.storage.sync.get(onSyncedStorageRecieved);
            } else {

                let gettingItem2 = browser.storage.sync.get();
                gettingItem2.then(onSyncedStorageRecieved, onError);
            }
        }

        function allowStartWindows(windowInfoArray) {//init1
            for (let windowInfo of windowInfoArray) {
                allowWindow(windowInfo.id);
            }

            //start init2
            if (mybrowser === "chrome") {
                browser.storage.local.get(onLocalStorageReceived);
            } else {
                let gettingItem = browser.storage.local.get();
                gettingItem.then(onLocalStorageReceived, onError);

            }


        }


        if (mybrowser === "chrome") {
            browser.windows.getAll({
                populate: true,
                windowTypes: ["normal"]
            }, allowStartWindows);
        } else {
            var getting = browser.windows.getAll({
                populate: true,
                windowTypes: ["normal"]
            });
            getting.then(allowStartWindows, onError);

        }

        function log(text) {
            plattform.logger.log(text);
        }
    }
}