cd ..\..\
xcopy /E /I /Y src\resources target\resources
mkdir target\main\js
copy src\main\js\script.js target\main\js\script.js
copy src\main\js\storageGet.js target\main\js\storageGet.js
copy src\main\js\storageSet.js target\main\js\storageSet.js

rem copy config\manifest\manifest.json target\manifest.json

cd config\build\