# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project does adhere to [Semantic Versioning](http://semver.org/).

---
## [2.1.3] - 2022-08-25

### Added
- 
### Changed
- 

### Fixed
- fixed: settings layout + labels

---

## [2.1.2] - 2022-08-25

### Added
- 
### Changed
- 

### Fixed
-  fixed: On macOS the first window is some times blocked

---

## [2.1.1] - 2022-08-25

### Added
- Chrome compatibility
### Changed
- 

### Fixed
- fixed: private window is blocked, when user allowed execution in private
- [Beta] fixed: Description of "New Window Allowlist"

---

## [2.1.0] - 2021-08-04

### Added
- [Beta] Added New Allowlist for target window

### Changed
- renamed lists

### Fixed
- fixed typos
- [Beta] manually opened windows are not blocked if "New Allowlist" is enabled

---

## [2.0.1] - 2018-09-16

### Added
- 
### Changed
- 

### Fixed
- fixed: blacklist and whitelist empty themselves

---

## [2.0.0] - 2017-11-14

### Added
- Neu: Blacklist/Whitelist mit getrennten Einstellungsmöglichkeiten.

### Changed
- 

### Fixed
- 

---

## [1.2.1] - 2017-06-02

### Added
- neue "Smart Light" Option: mehr als ein Tab auf einmal erlauben

### Changed
- verbesserter "Smart" algorithmus
- Standardeinstellung auf "Smart Light" geändert

### Fixed
- bleibt jetzt auch bei Neustart des Bowsers deaktiviert

---

## [1.1.3] - 2017-05-24

### Added
- 

### Changed
- Standardeinstellung auf "Allow all new tabs" gesetzt

### Fixed
- 

---

## [1.1.2] - 2017-05-20

### Added
- Option um Webseiten daran zu hindern, neue Tabs zu öffnen.
- Smart" Option, die versucht ungewollt geöffnete Tab zu erkennen und zu blockieren

### Changed
- 

### Fixed
- Userinterface reagiert nicht im privaten Fenster.

---

## [1.0.8] - 2017-05-16

### Added
- 

### Changed
- 

### Fixed
- fixed: tab session restore

---

## [1.0.4] - 2017-05-16

### Added
- 

### Changed
- 

### Fixed
- bugfix

---

## [1.0.2] - 2017-05-13

### Added
- 

### Changed
- minor improvements

### Fixed
- 

---

## [1.0.0] - 2017-05-12

### Added
- initial Version

### Changed
- 

### Fixed
- 

---

