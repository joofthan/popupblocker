class State {
    public allowedWindows: any[];
    public recentOpenedTabs: any[];
    public noNewTabTill: number;
    public lastBlockedWindow: number;
    public lastUrlChange: number;
    public countSec: number;
    public isWatching: boolean;
    public myWatchTab: any;
    public myActiveTab: any;
    public myCurrentUrl: string;
    constructor() {
        this.allowedWindows = [];
        this.recentOpenedTabs = [];

        this.noNewTabTill  = 0;
        this.lastBlockedWindow = 0;
        this.lastUrlChange = 0;
        this.countSec = 0;
        this.isWatching = false;
        this.myWatchTab;
        this.myActiveTab;
        this.myCurrentUrl;
    }


}