class ListManager {
	private blacklist: any[];
	private whitelist: any[];
	private advancedwhitelist: any[];
	private advancedwhitelistDefaults = ["about:home","about:privatebrowsing", "moz-extension://", "chrome://newtab/"];
	private logger: Logger;

	constructor(logger:Logger) {
		this.blacklist = [];
		this.whitelist = [];
		this.advancedwhitelist = this.advancedwhitelistDefaults;
		this.logger = logger;
		
	}

	addUrl(list, url) {
		let templist = this._returnChooseList(list);
		let temurl = this.getClearedUrl(url);
		if(temurl === "")return false;
		templist.push(temurl)
		return true;
	}
	
	inList(list, activeurl){
		let templist = this._returnChooseList(list);
		let activepage = this.getClearedUrl(activeurl);
		for (const v in templist) {
			let patternString = templist[v];
			if(patternString == activepage){
				return true;
			}
			try {
				const re = new RegExp(patternString.replace("*","(.*)"));
				if(re.exec(activepage) != null){
					return true
				}
			}catch (e){
				this.logger.log(e)
			}

		}
		return false;
	}
	getList(list){
		let templist = this._returnChooseList(list);
		
		let returni = "";
		for(let key in templist) {
			if(templist.hasOwnProperty(key)) {
				returni = returni + key + "\n";
			}
		}
		return returni;
	}
	
	_returnChooseList(listname){
		if(listname === "blacklist") return this.blacklist;
		if(listname === "whitelist") return this.whitelist;
		if(listname === "advancedwhitelist") return this.advancedwhitelist;
		return null;
	}
	
	
	getClearedUrl(longurl){
		if (longurl === null) return null;
		return longurl.replace("http://","").replace("https://","").replace("www.","").split("/")[0];
	}


	loadSyncedSettings(item){
		this._loadList("blacklist", item["blacklist"]);
		this._loadList("whitelist", item["whitelist"]);
		this._loadList("advancedwhitelist", item["advancedwhitelist"]);
	}

	_loadList(id, listAsString) {
		if (listAsString !== undefined) {
			let urlArray = listAsString.split("\n");
			for (let i = 0; i < urlArray.length; i++) {
				this.addUrl(id, urlArray[i]);
			}
		}
	}

	loadLists(changes) {
		if(changes["blacklist"] !== undefined){
			this.blacklist = [];
			this._loadList("blacklist", changes["blacklist"].newValue);
		}
		if(changes["whitelist"] !== undefined) {
			this.whitelist = [];
			this._loadList("whitelist", changes["whitelist"].newValue);
		}
		if(changes["advancedwhitelist"] !== undefined) {
			this.advancedwhitelist = this.advancedwhitelistDefaults;
			this._loadList("advancedwhitelist", changes["advancedwhitelist"].newValue);
		}
	}
}