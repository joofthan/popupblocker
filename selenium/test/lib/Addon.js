const { By } = require('selenium-webdriver');



class Addon {
    static START_TIMEOUT = 11;
    static MAX_RESET_DELAY = 11;
    driver;
    robot;

    constructor(driver, robot, config){
        this.driver = driver;
        this.robot = robot;
        this.c = config;
    }

    static async create(driver, robot, config){
        let addon = new Addon(driver, robot, config);
        for (let index = 0; index < addon.c.init.length; index++) {
            const elementPos = addon.c.init[index];
            await addon.click(elementPos);
        }
        return addon;
    }

    async clickSelector(selector){
        await this.driver.findElement(By.css(selector)).click();
    }

    async click(confElem){
        await this.robot.click(confElem[0], confElem[1]);
        if(confElem.length == 3){
            await this.driver.sleep(confElem[2]);
        } else {
            await this.driver.sleep(20);
        }
    }

    // [x, y, millisecondsAfterClick]
    static firefoxElementConfig = Addon.createElementConfig([1225, 60, 1000], 900);

    static chromeElementConfig = Addon.createElementConfig([1150, 60, 1000], 805, [
        [1130, 20, 1000], 
        [1100, 160, 1000],
        [1130, 20, 1000]
    ]);

    static createElementConfig(iconPos, xoffset, initPos = []) {
        return {
            icon: iconPos,
            enableNow: [xoffset+100, 200, 1000],
            addBlock: [xoffset+100, 95, 500],
            addAllow: [xoffset+200, 95, 500],
            blockAll: [xoffset+20, 155, 1000],
            smart: [xoffset+20, 178],
            smartLight: [xoffset+20, 205],
            allowAll: [xoffset+20, 230],
            moreSettings: [xoffset+325, 235, 1000],
            disable: [xoffset+20, 255, 1000],
            disable20: [xoffset+325, 255, 1000],
            init: initPos
        }
    }

    async clickIcon(){
        await this.click(this.c.icon);
    }
    async action(confElem){
        await this.click(this.c.icon);
        await this.click(confElem);
    }

    async enableNow(){
        await this.action(this.c.enableNow);
    }
    async addAllow(){
        await this.action(this.c.addAllow);
    }
    async addBlock(){
        await this.action(this.c.addBlock);
    }
    async blockAllTabs(){
        await this.action(this.c.blockAll);
        await this.click(this.c.icon);
    }
    async smartTabs(){
        await this.action(this.c.smart);
        await this.click(this.c.icon);
    }
    async smartLightTabs(){
        await this.action(this.c.smartLight);
        await this.click(this.c.icon);
    }
    async allowAllTabs(){
        await this.action(this.c.allowAll);
        await this.click(this.c.icon);
    }



}

module.exports = Addon;