const webdriver = require('selenium-webdriver');
const Addon = require("./Addon");
const { until } = require('selenium-webdriver');
const { By, Builder } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const chrome = require('selenium-webdriver/chrome');
const fs = require('fs');

const assert = require('assert');
const path = require('path');
const Page = require("./Page");
const Robot = require('./Robot');

class TestRunner {

    static offsetX = 50;
    static offsetY= 50;

    constructor(driver, robot, addon, debug) {
        this.driver = driver;
        this.robot = robot;
        this.page = new Page(driver);
        this.addon = addon;
        this.debug = debug;
    }

    async initialize(pathName) {
        let driver = this.driver;
        await driver.get(pathName);
        await driver.manage().window().setRect({x: TestRunner.offsetX, y: TestRunner.offsetY});
        await this.page.clickNewWindow();
        
        await driver.getAllWindowHandles().then(async function gotWindowHandles(allhandles) {
            await driver.switchTo().window(allhandles[allhandles.length - 1]);
            await driver.close();
            await driver.switchTo().window(allhandles[0]);
        });
    }

    getAddon(){
        return this.addon;
    }

    getPage(){
        return this.page;
    }

    async waitSeconds(s){
        await this.driver.sleep(s*1000);
    }

    async close(){
        await this.driver.quit();
    }

    async assertWindowCount(c){
        await this.countTitles('Tab', 'Tab Popup', 1);
        await this.countTitles('Window', 'Window Popup', c);
    }

    async assertTabCount(c){
        await this.countTitles('Tab', 'Tab Popup', c);
        await this.countTitles('Window', 'Window Popup', 1);
    }

    async countTitles(message, title, expectedCount) {
        let debug = this.debug;
        if(debug)console.log('Count '+message);
        let driver = this.driver;
        await this.driver.getAllWindowHandles().then(async function (windowHandles) {
            let count = 1;
            for (let i = 0; i < windowHandles.length; i++) {
                await driver.switchTo().window(windowHandles[i]);
                if(debug)console.log('  Handle Nr. '+i+': '+await driver.getTitle());
                if (await driver.getTitle() === title) {
                    count++;
                }
            }
            await assert.strictEqual(message+": " + count, message + ": " + expectedCount);
        });
    }

    async assertTitle(expected){
        let title = await this.driver.getTitle();
        await assert.strictEqual(title, expected);
    }
}

module.exports = TestRunner;