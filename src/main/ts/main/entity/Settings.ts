"use strict";

class Settings {
    public enabled: boolean;
    public tabblocksetting: string;
    public blockwindows: boolean;
    public tabblocksettingBlacklist: string;
    public blockwindowsBlacklist: boolean;
    public tabblocksettingWhitelist: string;
    public blockwindowsWhitelist: boolean;
    public waitforurl: boolean;

    constructor() {
        this.enabled = false;
        this.tabblocksetting;
        this.blockwindows;

        this.tabblocksettingBlacklist;
        this.blockwindowsBlacklist;

        this.tabblocksettingWhitelist;
        this.blockwindowsWhitelist;

        this.waitforurl;
    }


    loadLocalSettings(item) {
        if (item["tabblocksetting"] === undefined) this.tabblocksetting = "smartlight";
        else this.tabblocksetting = item["tabblocksetting"];
    }

    loadSyncedSettings(item) {
        if (item["blockwindows"] === undefined) this.blockwindows = true;
        else this.blockwindows = item["blockwindows"];
        if (item["blacklist_tabblocksetting"] === undefined)
            this.tabblocksettingBlacklist = "block";
        else this.tabblocksettingBlacklist = item["blacklist_tabblocksetting"];
        if (item["blacklist_blockwindows"] === undefined)
            this.blockwindowsBlacklist = true;
        else this.blockwindowsBlacklist = item["blacklist_blockwindows"];
        if (item["whitelist_tabblocksetting"] === undefined)
            this.tabblocksettingWhitelist = "allow";
        else this.tabblocksettingWhitelist = item["whitelist_tabblocksetting"];
        if (item["whitelist_blockwindows"] === undefined)
            this.blockwindowsWhitelist = false;
        else this.blockwindowsWhitelist = item["whitelist_blockwindows"];
        if (item["waitforurl"] === undefined)
            this.waitforurl = false;
        else this.waitforurl = item["waitforurl"];
    }

    loadSyncedListSettings(changes) {
        if (changes["blacklist_tabblocksetting"] !== undefined)
            this.tabblocksettingBlacklist = changes["blacklist_tabblocksetting"].newValue;
        if (changes["blacklist_blockwindows"] !== undefined)
            this.blockwindowsBlacklist = changes["blacklist_blockwindows"].newValue
        if (changes["whitelist_tabblocksetting"] !== undefined)
            this.tabblocksettingWhitelist = changes["whitelist_tabblocksetting"].newValue;
        if (changes["whitelist_blockwindows"] !== undefined)
            this.blockwindowsWhitelist = changes["whitelist_blockwindows"].newValue;
        if (changes["waitforurl"] !== undefined)
            this.waitforurl = changes["waitforurl"].newValue;
    }
}
