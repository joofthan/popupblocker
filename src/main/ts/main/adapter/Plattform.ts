class Plattform{
    private _browserName: string;
    private _browserPlattform: any;
    private _newTabString: string;
    private _isFirefox: boolean;
    logger: Logger;

    constructor(firefoxPlattform, chromePlattform, logger: Logger){
        this.logger = logger;
        if(firefoxPlattform !== null){
            this._browserName = "firefox"
            this._browserPlattform = firefoxPlattform;
            this._newTabString = "about:blank";
            this._isFirefox = true;
        }else if (chromePlattform !== null) {
            this._browserName = "chrome"
            this._browserPlattform = chromePlattform;
            this._newTabString = "";
            this._isFirefox = false;
        } else{
            throw new Error("Plattform is not supported. Only Firefox and Chrome");
        }
    }

    getBrowser(){
        return this._browserPlattform;
    }

    getBrowserName(){
        return this._browserName;
    }

    getNewTabString(){
        return this._newTabString;
    }

    isFirefox(){
        return this._isFirefox;
    }

    getTabHandler(){
        return new TabHandler(this);
    }

    static getInstance(browser, chrome, logger){
        return new Plattform(browser, chrome, logger);
    }

    setDevMode(devMode: boolean) {
        this.logger.devMode = devMode;
        if(devMode){
            this.logger.log("Running in devMode");
            //this.logger.log(this._browserPlattform.extension.getURL("resources/icons/icon-96.png"));
            ///alert("Running in devMode");
            this._browserPlattform.notifications.create("isinlist", {
                "type": "basic",
                "iconUrl": "resources/icons/icon-96.png",
                "title": "Popupblocker",
                "message": 'Running in devMode.'
            });
        }
        
    }
}
