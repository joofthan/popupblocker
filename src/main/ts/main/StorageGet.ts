class StorageGet{
	run(plattform){
		var mybrowser = plattform.getBrowserName();
		var browser = plattform.getBrowser();


		var defaultElement  = document.getElementById("smartlight") as HTMLInputElement;
		var blockwindowsElement = document.getElementById("blockwindows") as HTMLInputElement;

		var defaultTabblocksettingBlacklistElement = document.getElementById("black-block") as HTMLInputElement;
		var blockwindowsBlacklistElement = document.getElementById("black-blockwindows") as HTMLInputElement;

		var defaultTabblocksettingWhitelistElement = document.getElementById("white-allow") as HTMLInputElement;
		var blockwindowsWhitelistElement = document.getElementById("white-blockwindows") as HTMLInputElement;


		var blacklistElement = document.getElementById("blacklist") as HTMLInputElement;
		var whitelistElement = document.getElementById("whitelist") as HTMLInputElement;
		var advancedwhitelistElement = document.getElementById("advancedwhitelist") as HTMLInputElement;
		var waitforurlElement = document.getElementById("waitforurl") as HTMLInputElement;
		var waitforurloptionsElement = document.getElementById("waitforurloptions") as HTMLInputElement;

		if(mybrowser === "chrome"){
			browser.storage.local.get(onLocalStorageRecieved);
			browser.storage.sync.get(onSyncedStorageReceived);
		}else{
			let gettingItem = browser.storage.local.get();
			gettingItem.then(onLocalStorageRecieved, onErrors);

			let gettingItem2 = browser.storage.sync.get();
			gettingItem2.then(onSyncedStorageReceived, onErrors);
		}

		function onLocalStorageRecieved(item) {
			if(defaultElement != null){
				if(item["tabblocksetting"] === undefined)defaultElement.checked = true;
				else { // @ts-ignore
					document.getElementById(item["tabblocksetting"]).checked = true;
				}
			}

		}

		function onSyncedStorageReceived(item) {

			if(blacklistElement != null){
				if(item["blacklist"] === undefined)blacklistElement.value = "";
				else blacklistElement.value = item["blacklist"];
			}

			if(whitelistElement != null){
				if(item["whitelist"] === undefined)whitelistElement.value = "";
				else whitelistElement.value = item["whitelist"];
			}

			if(advancedwhitelistElement != null){
				if(item["advancedwhitelist"] === undefined)advancedwhitelistElement.value = "startpage.com\ngoogle.com";
				else advancedwhitelistElement.value = item["advancedwhitelist"];
			}
			if(waitforurlElement != null){
				if(item["waitforurl"] === undefined) waitforurlElement.checked = false;
				else waitforurlElement.checked = item["waitforurl"];

				if(waitforurlElement.checked) {
					waitforurloptionsElement.style.height = "initial";
					waitforurloptionsElement.style.overflow = "visible";
					waitforurloptionsElement.style.display = "block";
				} else{
					waitforurloptionsElement.style.height = "0";
					waitforurloptionsElement.style.overflow = "hidden";
					waitforurloptionsElement.style.display = "none";
				}
			}


			if(blockwindowsElement != null){
				if(item["blockwindows"] === undefined)blockwindowsElement.checked = true;
				else blockwindowsElement.checked = item["blockwindows"];
			}




			if(defaultTabblocksettingBlacklistElement != null){
				if(item["blacklist_tabblocksetting"] === undefined)defaultTabblocksettingBlacklistElement.checked = true;
				else { // @ts-ignore
					document.getElementById("black-" + item["blacklist_tabblocksetting"]).checked = true;
				}
			}

			if(blockwindowsBlacklistElement != null){
				if(item["blacklist_blockwindows"] === undefined)blockwindowsBlacklistElement.checked = true;
				else blockwindowsBlacklistElement.checked = item["blacklist_blockwindows"];
			}



			if(defaultTabblocksettingWhitelistElement != null){
				if(item["whitelist_tabblocksetting"] === undefined)defaultTabblocksettingWhitelistElement.checked = true;
				else { // @ts-ignore
					document.getElementById("white-" + item["whitelist_tabblocksetting"]).checked = true;
				}
			}


			if(blockwindowsWhitelistElement != null){
				if(item["whitelist_blockwindows"] === undefined)blockwindowsWhitelistElement.checked = false;
				else blockwindowsWhitelistElement.checked = item["whitelist_blockwindows"];
			}
		}

		function onErrors(error) {
			console.log(error);
			//defaultElement.checked = true;
		}



		browser.storage.onChanged.addListener(function(){
			window.location.reload();
		});

	}
}