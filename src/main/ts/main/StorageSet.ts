class StorageSet {
	run(plattform){
		var mybrowser = plattform.getBrowserName();
		var browser = plattform.getBrowser();

		var blockwindowsElement = document.getElementById("blockwindows") as HTMLInputElement;
		var page = browser.extension.getBackgroundPage();

		document.addEventListener("click", (event) => {
			let e = {target:(<HTMLTextAreaElement>event.target)};//typescript fix
			if (e.target.classList.contains("newWindow")) {
				//if(page.isEnabled())page.setAllowNext(true);
				//browser.windows.create({});
				//window.close();
			}
			else if (e.target.classList.contains("addBlacklist")) {
				browser.storage.local.set({
					addonfunction:  "addBlacklist"
				});
				window.close();
			}
			else if (e.target.classList.contains("addWhitelist")) {
				browser.storage.local.set({
					addonfunction:  "addWhitelist"
				});
				window.close();
			}
			else if (e.target.classList.contains("allowNext")||e.target.classList.contains("allowNextinSetting")) {
				browser.storage.local.set({
					addonfunction:  "allowNext"
				});
				window.close();
			}
			else if (e.target.classList.contains("moresetting")) {
				browser.storage.local.set({
					addonfunction:  "moresetting"
				});
				window.close();
			}
			else if (e.target.classList.contains("cancel")) {
				browser.storage.local.set({
					addonfunction:  "cancel"
				});
				window.close();
			}
			else if (e.target.classList.contains("enable")) {
				browser.storage.local.set({
					addonfunction:  "enable",
					addonstatus:  "enabled"
				});
				window.close();
			}
			else if (e.target.classList.contains("disable")) {
				browser.storage.local.set({
					addonfunction:  "disable",
					addonstatus:  "disabled"
				});
				window.close();
			}else if (e.target.classList.contains("feedback")) {
				browser.storage.local.set({
					addonfunction:  "feedback"
				});
				window.close();
			}
			else if (e.target.classList.contains("block")) {
				browser.storage.local.set({
					tabblocksetting:  "block"
				});
			}else if (e.target.classList.contains("smart")) {
				browser.storage.local.set({
					tabblocksetting:  "smart"
				});
			}else if (e.target.classList.contains("smartlight")) {
				browser.storage.local.set({
					tabblocksetting:  "smartlight"
				});
			}else if (e.target.classList.contains("allow")) {
				browser.storage.local.set({
					tabblocksetting:  "allow"
				});



			}else if (e.target.classList.contains("black-block")) {
				browser.storage.sync.set({
					blacklist_tabblocksetting:  "block"
				});
			}else if (e.target.classList.contains("black-smart")) {
				browser.storage.sync.set({
					blacklist_tabblocksetting:  "smart"
				});
			}else if (e.target.classList.contains("black-smartlight")) {
				browser.storage.sync.set({
					blacklist_tabblocksetting:  "smartlight"
				});
			}else if (e.target.classList.contains("black-allow")) {
				browser.storage.sync.set({
					blacklist_tabblocksetting:  "allow"
				});



			}else if (e.target.classList.contains("white-block")) {
				browser.storage.sync.set({
					whitelist_tabblocksetting:  "block"
				});
			}else if (e.target.classList.contains("white-smart")) {
				browser.storage.sync.set({
					whitelist_tabblocksetting:  "smart"
				});
			}else if (e.target.classList.contains("white-smartlight")) {
				browser.storage.sync.set({
					whitelist_tabblocksetting:  "smartlight"
				});
			}else if (e.target.classList.contains("white-allow")) {
				browser.storage.sync.set({
					whitelist_tabblocksetting:  "allow"
				});
			}
		});



		document.addEventListener("change",listChange);


		function listChange(e){
			if (e.target.classList.contains("blacklist")) {
				browser.storage.sync.set({
					blacklist:  e.target.value
				});
			}
			if (e.target.classList.contains("whitelist")) {
				browser.storage.sync.set({
					whitelist:  e.target.value
				});
			}

			if (e.target.classList.contains("advancedwhitelist")) {
				browser.storage.sync.set({
					advancedwhitelist:  e.target.value
				});
			}

			if (e.target.classList.contains("blockwindows")) {
				browser.storage.sync.set({
					blockwindows:  blockwindowsElement.checked
				});
			}

			if (e.target.classList.contains("black-blockwindows")) {

				browser.storage.sync.set({// @ts-ignore
					blacklist_blockwindows:  document.getElementById("black-blockwindows").checked
				});
			}

			if (e.target.classList.contains("white-blockwindows")) {

				browser.storage.sync.set({// @ts-ignore
					whitelist_blockwindows:  document.getElementById("white-blockwindows").checked
				});
			}

			if (e.target.classList.contains("waitforurl")) {
				browser.storage.sync.set({// @ts-ignore
					waitforurl:  document.getElementById("waitforurl").checked
				});
			}

		}
	}
}