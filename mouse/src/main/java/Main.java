import java.awt.*;
import java.awt.event.InputEvent;

public class Main{
    public static void main(String[] args) throws AWTException {
        Robot robot = new Robot();
        robot.mouseMove(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
        robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
        System.out.println("Clicked on "+args[0]+" " + args[1]);
    }
}