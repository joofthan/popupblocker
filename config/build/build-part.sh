cd ../../
mkdir target/
mkdir target/resources/
cp -r src/resources/* target/resources/
mkdir target/main/
mkdir target/main/js
cp src/main/js/script.js target/main/js/script.js
cp src/main/js/storageGet.js target/main/js/storageGet.js
cp src/main/js/storageSet.js target/main/js/storageSet.js

cd config/build/