
class TabHandler {
    private plattform: Plattform;
    constructor(plattform){
        this.plattform = plattform;
    }
    getActiveTab(callback){
        if(this.plattform.isFirefox()){
            let gettingCurrent = this.plattform.getBrowser().tabs.query({'active': true, 'lastFocusedWindow': true});
            gettingCurrent.then(callback, this._onError);
        }else{
            this.plattform.getBrowser().tabs.query({'active': true, 'lastFocusedWindow': true}, callback);
        }
    }

    getActiveWindow(callback){
        if(this.plattform.isFirefox()){
            var querying = this.plattform.getBrowser().tabs.query({currentWindow: true, active: true});
            querying.then(callback, this._onError);
        }else{
            var querying = this.plattform.getBrowser().tabs.query({currentWindow: true, active: true}, callback);
        }
    }

    async getWindowCount():Promise<number> {
        if(this.plattform.isFirefox()) {
            let wind = await this.plattform.getBrowser().windows.getAll();
            return wind.length;
        }else{
            let pr = new Promise<number>((resolve)=>{
                this.plattform.getBrowser().windows.getAll((winds)=>{
                    resolve(winds.length);
                });
            });
            return pr;
        }
    }

    _onError(error){
        this.plattform.logger.log(`Error: ${error}`);
    }
}