let mocha = require('mocha');
let describe=mocha.describe;
let it =mocha.it;
let expect = require("chai").expect;
let fs = require('fs');
let vm = require('vm');
let code = fs.readFileSync('./../../../target/main/js/bundle.js');
vm.runInThisContext(code);


const blocklist = "blacklist";

describe('ListManager', ()=>{
    it('should contain url when added', ()=>{
        let listManager = new ListManager(new Logger());
        listManager.addUrl(blocklist,"http://test.com");
        let inList = listManager.inList(blocklist,"test.com");
        expect(inList).to.be.true;
        let notInList = listManager.inList(blocklist,"test2.com");
        expect(notInList).to.be.false;
    });

    it('should contain url when wildcards are used', ()=>{
        let listManager = new ListManager(new Logger());
        listManager.addUrl(blocklist,"http://127.0.0.1:*");
        let inList = listManager.inList(blocklist,"127.0.0.1:8080");
        expect(inList).to.be.true;
        let notInList = listManager.inList(blocklist,"127.0.0.2:8080");
        expect(notInList).to.be.false;
    });
    it('should contain url when wildcards are used in front', ()=>{
        let listManager = new ListManager(new Logger());
        listManager.addUrl(blocklist,"*.website.com");
        let inList = listManager.inList(blocklist,"www1.website.com");
        expect(inList).to.be.true;
        let notInList = listManager.inList(blocklist,"www2.webb.com");
        expect(notInList).to.be.false;
    });


});