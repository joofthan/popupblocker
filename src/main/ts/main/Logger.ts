class Logger {
    devMode = false;
    log(text: string) {
        if(this.devMode){
            console.error(text);
        }
    }
}