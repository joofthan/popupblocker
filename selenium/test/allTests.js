const TestRunner = require('./lib/TestRunner');
const Addon = require('./lib/Addon');
const Page = require('./lib/Page');
const TestRunnerFactory = require('./lib/TestRunnerFactory');

let browserName = process.env['BROWSER_NAME'];
let debug = false;
const D = 35000;

describe('Tests with enableNow (without smart and smartLight)', () => {
    let r= new TestRunner();//for Autocomplete
    let addon = new Addon();
    let page = new Page();

    beforeEach(async () => {
        r = await TestRunnerFactory.create(browserName, debug);
        addon = r.getAddon();
        page = r.getPage();
        await addon.enableNow();
    });
  
    afterEach(async () => {
      await r.close();
    }, 40000);


    it('test all actions', async ()=>{
        let sec = 1;
        await addon.addAllow();
        await r.waitSeconds(sec);
        await addon.addBlock();
        await r.waitSeconds(sec);
        await addon.blockAllTabs();
        await r.waitSeconds(sec);
        await addon.smartTabs();
        await r.waitSeconds(sec);
        await addon.smartLightTabs();
        await r.waitSeconds(sec);
        await addon.allowAllTabs();
        await r.waitSeconds(sec);
    }, D);

    it('should allow tab on allowlist', async () => {
        await addon.addAllow();
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);

    it('should allow window on allowlist', async () => {
        await addon.addAllow();
        await page.clickNewWindow();
        await r.assertWindowCount(2);
    }, D);

    it('should block tab on blocklist', async () => {
        await addon.addBlock();
        await page.clickNewTab();
        await r.assertTabCount(1);
    }, D);

    it('should block window on blocklist', async () => {
        await addon.addBlock();
        await page.clickNewWindow();
        await r.assertWindowCount(1);
    }, D);

    it('should block tab when blockAllTabs', async () => {
        await addon.blockAllTabs();
        await page.clickNewTab();
        await r.assertTabCount(1);
    }, D);

    it('should allow tabs when allowAllTabs', async () => {
        await addon.allowAllTabs();
        await page.clickNewTab();
        await page.clickNewTab();
        await page.clickNewTab();
        await r.assertTabCount(4);
    }, D);

});


describe('Tests with smartTabs', () => {
    let r= new TestRunner();//for Autocomplete
    let addon = new Addon();
    let page = new Page();

    beforeEach(async () => {
        r = await TestRunnerFactory.create(browserName,debug);
        addon = r.getAddon();
        page = r.getPage();
        await addon.enableNow();
        await addon.smartTabs();
    });
  
    afterEach(async () => {
      await r.close();
    }, 40000);

    it('should allow one tab when smartTabs', async () => {
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);

    it('should block two tabs when smartTabs', async () => {
        await page.clickNewTab();
        await page.clickNewTab();
        await r.assertTabCount(1);
    }, D);

    it('should block window and tab when smartTabs', async () => {
        await page.clickNewTab();
        await page.clickNewWindow();
        await r.assertTabCount(1);
    }, D);

    it('should close previous tab after new window when smartTabs', async () => {
        await page.clickNewTab();
        await page.clickNewWindow();
        await r.assertTabCount(1);
    }, D);

    it('should allow one tab after timeout when smartTabs', async () => {
        await page.clickNewTab();
        await page.clickNewWindow();
        await r.assertTabCount(1);
        await r.waitSeconds(Addon.MAX_RESET_DELAY);
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);
});


describe('Tests with smartLightTabs', () => {
    let r= new TestRunner();//for Autocomplete
    let addon = new Addon();
    let page = new Page();

    beforeEach(async () => {
        r = await TestRunnerFactory.create(browserName,debug);
        addon = r.getAddon();
        page = r.getPage();
        await addon.enableNow();
        await addon.smartLightTabs();
    });
  
    afterEach(async () => {
      await r.close();
    }, 40000);

    it('should allow one tab when smartLightTabs', async () => {
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);

    it('should allow two tabs when smartLightTabs', async () => {
        await page.clickNewTab();
        await page.clickNewTab();
        await r.assertTabCount(3);
    }, D);

    it('should block window and Tab when smartLightTabs', async () => {
        await page.clickNewTab();
        await page.clickNewWindow();
        await r.assertTabCount(1);
    }, D);

    it('should block tab and window when smartLightTabs', async () => {
        await page.clickNewTab();
        await page.clickNewWindow();
        await r.assertTabCount(1);
    }, D);

    it('should allow one tab after timeout when smartLightTabs', async () => {
        await page.clickNewTab();
        await page.clickNewWindow();
        await r.assertTabCount(1);
        await r.waitSeconds(Addon.MAX_RESET_DELAY);
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);
});


describe('Tests before auto enabling', () => {
    let r= new TestRunner();//for Autocomplete
    let addon = new Addon();
    let page = new Page();

    beforeEach(async () => {
        r = await TestRunnerFactory.create(browserName,debug);
        addon = r.getAddon();
        page = r.getPage();
    }, 30000);

    afterEach(async () => {
      await r.close();
    }, 40000);

    it('should load title when testing', async () => {
      await r.assertTitle("Popupblocker Test");
    }, D);

    it('should allow window when settings not changed', async () => {
        await page.clickNewWindow();
        await r.assertWindowCount(2);
    }, D);

    it('should allow new tab when settings not changed', async () => {
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);
});


describe('Tests after auto enabling', () => {
    let r= new TestRunner();//for Autocomplete
    let addon = new Addon();
    let page = new Page();

    before(async () => {
        r = await TestRunnerFactory.create(browserName,debug);
        addon = r.getAddon();
        page = r.getPage();
    }, 30000);

    beforeEach(async () => {
        await r.waitSeconds(Addon.MAX_RESET_DELAY);
    });
  
    after(async () => {
      await r.close();
    }, 40000);

    it('should load title when testing', async () => {
      await r.assertTitle("Popupblocker Test");
    }, D);

    it('should block window when settings not changed', async () => {
        await page.clickNewWindow();
        await r.assertWindowCount(1);
    }, D);

    it('should allow new tab when settings not changed', async () => {
        await page.clickNewTab();
        await r.assertTabCount(2);
    }, D);
});
