const { By } = require('selenium-webdriver');

class Page {
    driver;

    constructor(driver){
        this.driver = driver;
    }

    async clickNewWindow(){
        await this.driver.findElement(By.id('newwindow')).click();
        await this.driver.sleep(100);
    }

    async clickNewTab(){
        await this.driver.findElement(By.id('newtab')).click();
        await this.driver.sleep(100);
    }
}

module.exports = Page;